#!/bin/sh

Kexts=("FakePCIID_HD4600_HD4400.kext" "FakePCIID.kext" "FakePCIID_Intel_HDMI_Audio.kext" "RealtekRTL8100.kext" "RealtekRTL8111.kext" "AtherosE2200Ethernet.kext"  "ApplePS2ElanTouchpad.kext" "ApplePS2ElanTouchpad.kext")
for kext in ${Kexts[@]}; 
do
  if [ -e "/Volumes/EFI/EFI/CLOVER/kexts/10.9/$kext" ]
  then
    rm -rf "/Volumes/EFI/EFI/CLOVER/kexts/10.9/$kext"
  fi

  if [ -e "/Volumes/EFI/EFI/CLOVER/kexts/10.10/$kext" ]
  then
    rm -rf "/Volumes/EFI/EFI/CLOVER/kexts/10.10/$kext"
  fi
done