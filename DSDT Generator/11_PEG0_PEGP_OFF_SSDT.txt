into_all method label _DSM remove_entry;

into method label WMMX code_regex Return\s\(\\_SB.PCI0.GFX0._DSM\)[^}]+\} replaceall_matched begin } end;

# rename GFX0
into_all all code_regex \.GFX0 replaceall_matched begin .IGPU end;
into_all all label \_SB.PCI0.GFX0 set_label begin \_SB.PCI0.IGPU end;

# call _OFF from _SB.PCI0.PEG0.PEGP._INI
into method label _INI parent_label \_SB.PCI0.PEG0.PEGP code_regex . insert begin If (CondRefOf(\_SB_.PCI0.PEG0.PEGP._OFF)) { _OFF() }\n end;

